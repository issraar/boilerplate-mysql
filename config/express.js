
var helpers = require('view-helpers');
var pkg = require('../package.json');
var engine = require('ejs-locals');
var env = process.env.NODE_ENV || 'development'
var config = require('../config/config')[env]

// middlewares
var compression = require('compression')
var favicon = require('serve-favicon');
var serveStatic = require('serve-static')
var morgan       = require('morgan');

var session = require('express-session');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');


var bodyParser   = require('body-parser');
var methodOverride = require('method-override')

module.exports = function (app, config, passport) {

  app.set('showStackError', true);

  // should be placed before express.static
  app.use(compression({
    filter: function (req, res) {
      return /json|text|javascript|css/.test(res.getHeader('Content-Type'))
    },
    level: 9
  }));

  app.use(favicon(  config.root + '/public/favicon.ico'));
  app.use(serveStatic(config.root + '/public'));


  app.engine('ejs', engine);
  app.set('views', config.root + '/views');

  app.set('view engine', 'ejs'); // set up ejs for templating


  app.use(function (req, res, next) {
    res.locals.pkg = pkg;
    next();
  });

  app.use(morgan('dev')); // log every request to the console
  app.use(cookieParser('secret'));
  app.use(session({cookie: { maxAge: 60000 }}));
  app.use(flash());

  // bodyParser should be above methodOverride
  app.use(bodyParser.json()); // get information from html forms
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(methodOverride());


  // use passport session
  app.use(passport.initialize());
  app.use(passport.session());


  // should be declared after session and flash
  app.use(helpers(pkg.name));

  app.use(function(err, req, res, next){
    // treat as 404
    if (err.message
        && (~err.message.indexOf('not found')
        || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    }

    // log it
    // send emails if you want
    console.error(err.stack);

    // error page
    res.status(500).render('500', { error: err.stack });
  })

  require('../config/routes')(app, passport)

  // assume 404 since no middleware responded
  app.use(function(req, res, next){
    res.status(404).render('404', {
      url: req.originalUrl,
      error: 'Not found'
    })
  });


}
