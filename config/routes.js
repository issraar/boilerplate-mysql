var users = require('../app/controllers/users');
var auth = require('./middlewares/authorization');
var index= require('../app/controllers/index');


module.exports = function (app, passport) {

  app.get('/signin', users.showSignIn);

  app.get('/signup', users.showSignUp);

  app.get('/signout', users.signout);

  app.post('/users', users.create);

  app.post('/users/session',
    passport.authenticate('local', {
      failureRedirect: '/signin',
      failureFlash: 'Invalid email or password.'
    }), users.session);

  app.get('/users/:userId', users.showProfile);

  app.param('userId', users.user);

  app.get('/showtestuser', users.showtestuser);

  // home route
  app.get('/flash', function(req, res){
    req.flash('info', 'Flash is back!')
    res.redirect('/');
  });

  app.get('/', function(req, res){
    res.render('index', { messages: req.flash('info') });
  });

}
